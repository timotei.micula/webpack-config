 import ContactPage from './img/ContactPage.jpg';
import home from './img/home.jpg';
import Services from './img/Services.png';
import world from './img/world.png';
import _ from 'lodash';
import './style.css';

function component() {
    const element = document.createElement('div');
  
    //  element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    //  element.classList.add('hello');

   
   const myIcon = new Image();
   myIcon.src = ContactPage;

  //  element.appendChild(myIcon);

   const myIcon2 = new Image();
   myIcon2.src = home;

   const myIcon3 = new Image();
   myIcon3.src = Services;

   const myIcon4 = new Image();
   myIcon2.src = world;

  //  element.appendChild(myIcon2);
    return element;
  }
  
  document.body.appendChild(component());

  $(document).ready(function(){

    $(window).scroll(function(){
      if($(this).scrollTop() > 40){
        $('#topBtn').fadeIn();
      } else{
        $('#topBtn').fadeOut();
      }
    });
  
    $("#topBtn").click(function(){
      $('html ,body').animate({scrollTop : 0},800);
    });
  });
  
  
      // ===== End Scroll Top ===
  
     
  
        // Select all links with hashes
  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });