<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="Description"
    content="Traduceri autorizate. Birou traduceri. Traduceri legalizate. Traduceri de specialitate" />
  <meta name="keywords"
    content="Real Translations traduceri, real translations traduceri,real translations birou traduceri, real translations traduceri legalizate, real translations traduceri legalizate, Traduceri, Traducatori, traduceri autorizate, traduceri legalizate, Birou traduceri" />
  <meta name="RATING" content="General" />
  <meta name="ROBOTS" content="index,follow" />
  <meta name="googlebot" content="noarchive" />
  <meta name="robots" content="noarchive" />
  <meta name="verify-v1" content="WHXEKAcDwI08cf28k+Nb3MaR9lesmpOHCSfeqiNT/BE=" />


  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <link rel="stylesheet" href='../src/style.css'>
  
  <link rel="Shortcu Icon" href="./cb374dd1e66f101d171e2d5c47058193.png" />


  <title>Real Translations</title>
</head>

<body>
<!-- <?php

// require('lib/functions.php');


?> -->


  <nav class="navbar navbar-expand-lg fixed-top navbar-light  ">
    <a class="navbar-brand" href="#"><img class="logo" src="./cb374dd1e66f101d171e2d5c47058193.png" style="width:60px; " alt=""> </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">

         <li class="nav-item ">
          <a class="nav-link" href="#showcase"><i class="fas fa-home">HOME</i> </a>

        </li>
        <li class="nav-item">
          <a class="nav-link" href="#Services"><i class="fas fa-toolbox">&nbsp SERVICES</i></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="#price"><i class="fas fa-money-bill-wave-alt">&nbsp PRICE</i></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="#section-map"><i class="fas fa-id-card-alt">&nbsp CONTACT</i></a>
        </li>


      </ul>

    </div>
  </nav>

  <section   id="showcase">
   
    <div class="container">
      <br><br><br><br><br><br><br><br>
      <br>
      <h1 >Welcome!</h1>
      <p class="welcome"  > Our company's translators and interpreters,
      all graduated foreign languages faculties and they are authorized by the Ministry of Justice.
      
      Our company's translators and interpreters,
      all graduated foreign languages faculties and they are authorized by the Ministry of Justice.
    </p>
    </div>
  </section>

<section id="about"></section>

 
    
    
<section id="Services" >
  <div class="container">
    <br><br><br><br><br><br><br><br>
    <h1 >Services</h1>
      <p class="welcome" > <b>
        
          We offer you professional services of authorized translations 
          in/from more than 30 languages for the next domains:</b>
          Constructions,
  Agriculture,
  Mechanics,
  Transport,
  Electronics and communications,
  Medicine,
  Technique,
  IT,
  Economics,
  Personal and business correspondence,
  Files for emigration,
  Juridical acts and papers,
  Certificates,
  Promotional materials.
  
      </p>
  </div>
</section>



<!---------------------END Services --------------- -->

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "oyp";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT  * FROM price";
$result = $conn->query($sql);

?>



  <!-- Table -->
<section id="price">
  <h1 class="center">Price</h1>
  <div class="col-md-8 mx-auto">
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col"> &nbsp Language</th>
        <th scope="col">Comercial Text(RON) </th>
        <th scope="col">Tehnical Text(RON)</th>

      </tr>
    </thead>
    <tbody>
    <tr>

<?php

if ($result->num_rows > 0) {
// output data of each row
while($row = $result->fetch_assoc()) {
?>
 <!-- <td><?php  echo  'test'  ?></td> -->
  <td><?php  echo  $row["Language"]    ?></td>
  <td><?php  echo  $row["Comercial"]    ?></td>
  <td><?php  echo  $row["Tehnical"]  ?></td>
 
  

  
</tr>

<?php
}


} else {
echo "0 results";
}
$conn->close();



?> 

    </tbody>
  </table>
</div>
</section>

  <!-- Table -->






  <!-- map -->
  <section id="section-map" class="clearfix">
    <iframe
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d712.0041236348175!2d26.126308829241836!3d44.453332998693895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b1f8ea3361d4e9%3A0x16a96c654a23262a!2sReal%20Translations!5e0!3m2!1sro!2sus!4v1573065961704!5m2!1sro!2sus"
      width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

  </section>

  <!-- contact -->
  <section id="section-contact" class="section appear clearfix">
    <div class="container">

      <div class="row mar-bot40">
        <div class="offset-md-2 col-md-8">
          <div class="section-header" >
            <h2 class="section-heading animated" data-animation="bounceInUp">Contact us</h2>
            <p>You can find us everyday, <strong>Monday to Friday</strong>, from <strong>09:00-18:00</strong> at our
              headquarters, you can give us a <strong>call</strong>, or you can just <strong>e-mail</strong> us, for any
              concern or inquiry, using the contact form below. We are looking forward to working with you.</p>
            <ul class="adresa">
              <li> <i class="fas fa-map-marker-alt" ></i>&nbsp <strong  >ADDRESS:</strong> <p> Sos. Colentina, nr.7, bl. OD40,
                sc.1, et 2, ap.11, interfon 11C, Sector 2, Bucuresti (vis-a-vis de Kaufland Obor, langa Biserica Sf.
                Dumitru.<br>Metrou: Obor; Autobuz: 66; Tramvai: 21)</p>


              <i class="fas fa-mobile-alt" ></i>&nbsp&nbsp<strong >Mobil1:</strong><p > 0743.755.594 </p>
              <i class="fas fa-mobile-alt"></i>
              &nbsp&nbsp<strong >Mobil2:</strong> 
              <p> 0723.795.265</p> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="offset-md-2 col-md-8 ">
          <div class="cform" id="contact-form">
            <br><br><br>
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <form action="" method="post" class="contactForm">

              <div class="field your-name form-group">
                <input type="text" name="name" placeholder="Your Name" class="cform-text" size="40" data-rule="minlen:4"
                  data-msg="Please enter at least 4 chars">
                <div class="validation"></div>
              </div>
              <div class="field your-email form-group">
                <input type="text" name="email" placeholder="Your Email" class="cform-text" size="40" data-rule="email"
                  data-msg="Please enter a valid email">
                <div class="validation"></div>
              </div>
              <div class="field subject form-group">
                <input type="text" name="subject" placeholder="Subject" class="cform-text" size="40"
                  data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                <div class="validation"></div>
              </div>

              <div class="field message form-group">
                <textarea name="message" class="cform-textarea" cols="40" rows="10" data-rule="required"
                  data-msg="Please write something for us"></textarea>
                <div class="validation"></div>
              </div>

              <div class="send-btn">
                <input type="submit" value="SEND MESSAGE" style="border-radius: 19px;" class="btn btn-theme">
              </div>

            </form>
          </div>
        </div>
        
      </div>

    </div>
  </section>

    <button id="topBtn"  ><i class="fas fa-arrow-up"></i></button>
  
     
    


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="https://kit.fontawesome.com/afae9dfecb.js" crossorigin="anonymous"></script>

  <!-- for BtnTop -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="script.js"></script>
 <script src="index.js"></script>
 <script src="bundle.js"></script>
 <!-- end for BtnTop -->

 <!-- <script src="index.js"></script> -->
 
</body>

</html>